<?php

class m140307_070026_initial_table extends CDbMigration {

    public function safeUp() {
        $this->createTable('aing', array(
            'id'=>'pk',
            'name'=>'string'
        ));
    }

    public function safeDown() {
        $this->dropTable('aing');
    }

}